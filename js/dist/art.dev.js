"use strict";

var data = window.artData;
var _iteratorNormalCompletion = true;
var _didIteratorError = false;
var _iteratorError = undefined;

try {
  for (var _iterator = data[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
    var painting = _step.value;
    var project = document.createElement('art');
    project.className = 'project';
    project.innerHTML = ['<img class="project__image" src="', painting.src, '" />', '<div class="grid__overlay">', '<button class="viewbutton">', 'view more', '</button>', '</div>'].join('');
    document.getElementById('portfolio').appendChild(project);
  } // QUERY SELECTORS

} catch (err) {
  _didIteratorError = true;
  _iteratorError = err;
} finally {
  try {
    if (!_iteratorNormalCompletion && _iterator["return"] != null) {
      _iterator["return"]();
    }
  } finally {
    if (_didIteratorError) {
      throw _iteratorError;
    }
  }
}

var buttons = document.querySelectorAll('.project');
var overlayImage = document.querySelector('.overlay__inner img'); // OPEN FUNCTION

function open(e) {
  $('.overlay').toggleClass('open');
  var src = e.currentTarget.querySelector('img').src;
  var matchingDoc = data.filter(function (r) {
    return r.src.substring(r.src.length - 10, r.src.length) === src.substring(src.length - 10, src.length);
  });
  overlayImage.src = src;
  var imgWidth = overlayImage.width;

  var _content = matchingDoc[0].title + '</br>' + matchingDoc[0].footnote;

  $('.art_details').hide().html(_content).fadeIn();
  $('.art_details').attr({
    'width': imgWidth
  });
  $('.art_details').css('top', '0px');
  console.log(imgWidth);
} // CLOSING PICTURE


$(".overlay").click(function () {
  $(this).toggleClass("open");
  $('.art_details').css({
    'top': '50px'
  });
}); // ONCLICK EVENTS 

buttons.forEach(function (button) {
  return button.addEventListener('click', open);
});