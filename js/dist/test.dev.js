"use strict";

// Constants
var key = 'h9ur4OLfvghB1vMf71ef7d2TfZOZovTTMmKqFxLr';
var base_url = 'https://api.nasa.gov';
var apod = '/planetary/apod'; // Initial value

document.getElementById("date-input").valueAsDate = new Date(); // Calling endpoint

function getData(url) {
  var params, response;
  return regeneratorRuntime.async(function getData$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          params = {
            duration: 600
          };
          $('.apod-video').fadeOut(params);
          $('.apod-css').fadeOut(params);
          $('.apod-title').fadeOut(params);
          $('.apod-text').fadeOut(params);
          _context.next = 7;
          return regeneratorRuntime.awrap(fetch(url));

        case 7:
          response = _context.sent;
          return _context.abrupt("return", response.json());

        case 9:
        case "end":
          return _context.stop();
      }
    }
  });
}

function main() {
  var params, apod_day, url, data;
  return regeneratorRuntime.async(function main$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          params = {
            duration: 600
          };
          apod_day = document.getElementById("date-input").value;
          url = base_url + apod + '?api_key=' + key + '&date=' + apod_day + '&hd=true';
          _context2.next = 5;
          return regeneratorRuntime.awrap(getData(url));

        case 5:
          data = _context2.sent;
          _context2.next = 8;
          return regeneratorRuntime.awrap(new Promise(function (r) {
            return setTimeout(r, 600);
          }));

        case 8:
          if (data.media_type === 'video') {
            $('.apod-video').attr('src', data.url + "&autoplay=0&showinfo=0&controls=0").fadeIn(params);
            $('.apod-css').removeAttr('src');
          } else if (data.media_type === 'image') {
            $('.apod-css').attr('src', data.url).fadeIn(params);
            $('.apod-video').removeAttr('src');
          }

          $('.apod-title').html(data.title).fadeIn(params);
          $('.apod-text').html(data.explanation).fadeIn(params);

        case 11:
        case "end":
          return _context2.stop();
      }
    }
  });
}

main();