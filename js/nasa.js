// Constants
const key = 'h9ur4OLfvghB1vMf71ef7d2TfZOZovTTMmKqFxLr'
const base_url = 'https://api.nasa.gov'
const apod = '/planetary/apod'

// Initial value
document.getElementById("date-input").valueAsDate = new Date()

// Calling endpoint
async function getData(url) {
    const params = {duration: 600}
    $('.apod-video').fadeOut(params)
    $('.apod-css').fadeOut(params)
    $('.apod-title').fadeOut(params)
    $('.apod-text').fadeOut(params)
    $('.apod-date').fadeOut(params)
    const response = await fetch(url);
    return response.json()
}
async function main() {
    const params = {duration: 600}
    const apod_day = document.getElementById("date-input").value
    const url = base_url + apod + '?api_key=' + key + '&date=' + apod_day + '&hd=true';        
    const data = await getData(url);
    
    await new Promise(r => setTimeout(r, 600));

    if (data.media_type === 'video') {
        $('.apod-video').attr('src', data.url + "&autoplay=0&showinfo=0&controls=0").fadeIn(params)
        $('.apod-css').removeAttr('src')
    } else if (data.media_type ==='image') {
        $('.apod-css').attr('src', data.url).fadeIn(params)
        $('.apod-video').removeAttr('src')
    }

    $('.apod-date').fadeIn(params)
    $('.apod-title').html(data.title).fadeIn(params)
    $('.apod-text').html(data.explanation).fadeIn(params)   
}
main()
