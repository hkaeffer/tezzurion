const data = window.artData
for (var painting of data) {
    var project = document.createElement('art');
    project.className = 'project';
    project.innerHTML = [
        '<img class="project__image" src="', painting.src, '" />',
        '<div class="grid__overlay">',
        '<button class="viewbutton">', 'view more', '</button>',
        '</div>'
        ].join('');
    document.getElementById('portfolio').appendChild(project);
}


// QUERY SELECTORS
const buttons = document.querySelectorAll('.project');
const overlayImage = document.querySelector('.overlay__inner img');

// OPEN FUNCTION
function open(e) {
  $('.overlay').toggleClass('open');
  const src = e.currentTarget.querySelector('img').src;
  const matchingDoc = data.filter(r => 
    r.src.substring(r.src.length-10, r.src.length) === src.substring(src.length-10, src.length)
    )
  overlayImage.src = src;
  const imgWidth = overlayImage.width
  
  const _content = matchingDoc[0].title + '</br>' + matchingDoc[0].footnote
  $('.art_details').hide().html(_content).fadeIn();
  $('.art_details').attr({'width': imgWidth});
  $('.art_details').css('top', '0px');

  console.log(imgWidth)
  
}

// CLOSING PICTURE
$(".overlay").click(function() {
  $(this).toggleClass("open");
  $('.art_details').css({'top': '50px'})
});

// ONCLICK EVENTS 
buttons.forEach(button => button.addEventListener('click', open));

