"use strict";

var artData = [{
  "src": "../images/art-gallery/1.jpg",
  "title": "Dog 1",
  "footnote": "Footnote1"
}, {
  "src": "../images/art-gallery/2.jpg",
  "title": "Good bois running in the woods",
  "footnote": "They really are good boys"
}, {
  "src": "../images/art-gallery/3.jpg",
  "title": "Dog 3",
  "footnote": "Footnote3"
}, {
  "src": "../images/art-gallery/4.jpg",
  "title": "Dog 4",
  "footnote": "Footnote4"
}, {
  "src": "../images/art-gallery/5.jpg",
  "title": "Dog 5",
  "footnote": "Footnote5"
}, {
  "src": "../images/art-gallery/6.jpg",
  "title": "Dog 6",
  "footnote": "Footnote6"
}, {
  "src": "../images/art-gallery/7.jpg",
  "title": "Dog 7",
  "footnote": "Footnote7"
}, {
  "src": "../images/art-gallery/8.jpg",
  "title": "Dog 8",
  "footnote": "Footnote8"
}];